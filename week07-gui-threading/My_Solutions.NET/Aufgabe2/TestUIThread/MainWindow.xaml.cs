﻿using System.Threading.Tasks;
using System.Windows;

namespace TestUIThread {
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool _IsCancelled { get; set; }
        private bool _IsRunning { get; set; }

        private async void startCalculationButton_Click(object sender, RoutedEventArgs e) {
            if (_IsRunning)
            {
                _IsCancelled = true;
                _IsRunning = false;
            }
            else
            {
                long initial;
                long amount;

                if (!long.TryParse(baseNumberTextBox.Text, out initial) ||
                    !long.TryParse(succeedingPrimesTextBox.Text, out amount))
                {
                    return;
                }

                _IsRunning = true;
                startCalculationButton.Content = "Cancel";

                // async calculation
                await ComputeNextPrimesAsync(initial, amount);

                progressLabel.Content = _IsCancelled ? "cancelled" : "done";
                startCalculationButton.Content = "Start";
                _IsRunning = false;
                _IsCancelled = false;
            }
        }

        private async Task ComputeNextPrimesAsync(long inital, long amount) {
            for (var number = inital; number < inital + amount && _IsRunning; number++) {
                if (await IsPrime(number)) {
                    resultListView.Items.Add(number);
                }
                var progress = (number - inital + 1) * 100 / amount;
                progressLabel.Content = progress + "% computed";
            }
        }

        private Task<bool> IsPrime(long number)
        {
            return Task.Run(() =>
            {
                for (long i = 2; i * i <= number; i++)
                {
                    if (number % i == 0)
                    {
                        return false;
                    }
                }
                return true;
            });            
        }
    }
}
