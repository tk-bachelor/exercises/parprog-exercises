package parprog.week1;

class NeverEndingThread extends Thread {
  public void run() {
    try {
      while (true) {
        Thread.sleep(10000);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}

public class MaximumThreads {
  public static void main(String[] args) {
    int i = 0;
    while (true) {
      i++;
      new NeverEndingThread().start();
      System.out.println(i);
    }
  }
}
