package parprog.week1;

public class MultiThreadedPrimeCounter {
    private static boolean isPrime(long number) {
        for (long factor = 2; factor * factor <= number; factor++) {
            if (number % factor == 0) {
                return false;
            }
        }
        return true;
    }

    private static long countPrimes(long start, long end) {
        long count = 0;
        for (long number = start; number < end; number++) {
            if (isPrime(number)) {
                count++;
            }
        }
        return count;
    }

    static class PrimeCounterThread extends Thread {
        private final long start;
        private final long end;
        private final int threadNum;
        private final long[] resultArray;

        public PrimeCounterThread(int threadNum, long[] resultArray, long start, long end) {
            this.setName("PrimeCounterThread " + threadNum);
            this.threadNum = threadNum;
            this.resultArray = resultArray;

            this.start = start;
            this.end = end;
        }

        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            System.out.println(String.format("%s started running", this.getName()));
            resultArray[threadNum] = countPrimes(start, end);
            long endTime = System.currentTimeMillis();

            System.out.println(String.format("%s found %d prime numbers. Time %dms", this.getName(), resultArray[threadNum], (endTime - startTime)));
        }
    }

    private static final long START = 1_000_000L;
    private static final long END = 10_000_000L;

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();

        final int NUM_THREADS = Runtime.getRuntime().availableProcessors();

        Thread[] threadArray = new Thread[NUM_THREADS];
        final long[] countArray = new long[NUM_THREADS];

        final long range = (END - START) / NUM_THREADS;

        for (int t = 0; t < NUM_THREADS; t++) {
            // calculate range
            final long threadStart = START + t * range;
            final long threadEnd = threadStart + START;

            Thread thread = new PrimeCounterThread(t, countArray, threadStart, threadEnd);
            threadArray[t] = thread;
            thread.start();
        }

        int count = 0;
        for (int t = 0; t < NUM_THREADS; t++) {
            threadArray[t].join();
            count += countArray[t];
        }
        long endTime = System.currentTimeMillis();
        System.out.println("#Primes: " + count + " Time: " + (endTime - startTime) + " ms");

        /**
         * OUTPUT:

         PrimeCounterThread 6 started running
         PrimeCounterThread 3 started running
         PrimeCounterThread 5 started running
         PrimeCounterThread 7 started running
         PrimeCounterThread 2 started running
         PrimeCounterThread 4 started running
         PrimeCounterThread 0 started running
         PrimeCounterThread 1 started running

         PrimeCounterThread 0 found 70435 prime numbers. Time 1761ms
         PrimeCounterThread 1 found 67654 prime numbers. Time 2211ms
         PrimeCounterThread 2 found 66006 prime numbers. Time 2752ms
         PrimeCounterThread 3 found 65004 prime numbers. Time 2915ms
         PrimeCounterThread 4 found 63957 prime numbers. Time 3185ms
         PrimeCounterThread 5 found 63395 prime numbers. Time 3272ms
         PrimeCounterThread 6 found 62824 prime numbers. Time 3532ms
         PrimeCounterThread 7 found 62238 prime numbers. Time 3776ms

         #Primes: 521513 Time: 3778 ms

         */
    }
}
