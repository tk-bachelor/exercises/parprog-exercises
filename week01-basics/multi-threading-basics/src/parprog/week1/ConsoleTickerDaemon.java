package parprog.week1;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleTickerDaemon {
    private static void periodTicker(char sign, int intervallMillis) throws InterruptedException {
        while (true) {
            System.out.print(sign);
            Thread.sleep(intervallMillis);
        }
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        // calling this function in the beginning will block the rest the statements from executing.
        // periodTicker('.', 10);

        // Concurrent 1: Thread-Subclass
        Thread threadA = new TickerThread();
        threadA.setName("A-ConsoleTicker");
        threadA.setDaemon(true);
        threadA.start();

        // Concurrent 2: Runnable implementation
        Thread threadB = new Thread(new TickerRunnableLogic());
        threadB.setName("B-ConsoleTicker");
        threadB.setDaemon(true);
        threadB.start();

        // Concurrent 3: Java 8 Lamda
        Thread threadC = new Thread(() -> {
            try {
                periodTicker('~', 100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        threadC.setName("C-ConsoleTicker");
        threadC.setDaemon(true);
        threadC.start();

        // Concurrent 4: Anonymous Class
        Thread threadD = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    periodTicker('&', 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        threadD.setName("D-ConsoleTicker");
        threadD.setDaemon(true);
        threadD.start();

        threadA.interrupt();
        System.in.read();


        // NOTE: Daemon-Thread behavior
        /**
         * If all of the non-daemon threads have died, JVM will stop all the daemon threads as well.
         *
         * When Thread A,B,C and D were marked as daemon, they printed the character only once and stopped running.
         * However, as one thread was NOT running as a daemon thread, the program continued running forever.
         */
    }

    static class TickerThread extends Thread {
        @Override
        public void run() {
            try {
                periodTicker('*', 100);
            } catch (InterruptedException e) {
                System.out.println("TickerThread is interrupted");
                e.printStackTrace();
            }
        }
    }

    static class TickerRunnableLogic implements Runnable{
        @Override
        public void run() {
            try {
                periodTicker('\\', 100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally{
                // This line of code will not be executed IF JVM stops the thread
                // but threadB.interrupt() would do the trick
                System.out.println("TicketRunnableLogic has stopped");
            }
        }
    }
}
