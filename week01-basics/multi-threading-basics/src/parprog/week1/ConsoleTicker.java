package parprog.week1;

public class ConsoleTicker {
    private static void periodTicker(char sign, int intervallMillis) throws InterruptedException {
        while (true) {
            System.out.print(sign);
            Thread.sleep(intervallMillis);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // calling this function in the beginning will block the rest the statements from executing.
        // periodTicker('.', 10);

        // Concurrent 1: Thread-Subclass
        Thread threadA = new TickerThread();
        threadA.setName("A-ConsoleTicker");
        threadA.start();

        // Concurrent 2: Runnable implementation
        Thread threadB = new Thread(new TickerRunnableLogic());
        threadB.setName("B-ConsoleTicker");
        threadB.start();

        // Concurrent 3: Java 8 Lamda
        Thread threadC = new Thread(() -> {
            try {
                periodTicker('~', 10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        threadC.setName("C-ConsoleTicker");
        threadC.start();

        // Concurrent 4: Anonymous Class
        Thread threadD = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    periodTicker('&', 10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        threadD.setName("D-ConsoleTicker");
        threadD.start();
    }

    static class TickerThread extends Thread {
        @Override
        public void run() {
            try {
                periodTicker('*', 10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class TickerRunnableLogic implements Runnable{
        @Override
        public void run() {
            try {
                periodTicker('\\', 10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
