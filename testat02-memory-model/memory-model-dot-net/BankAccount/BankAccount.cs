﻿using System;
using System.Threading;

namespace BankAccount
{
    public class BankAccount
    {
        private int _balance = 0;

        public int Balance => _balance;

        public void Deposit(int amount)
        {
            Interlocked.Add(ref _balance, amount);
        }

        public bool Withdraw(int amount)
        {

            if (amount <= Balance)
            {
                Interlocked.Add(ref _balance, -amount);
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
