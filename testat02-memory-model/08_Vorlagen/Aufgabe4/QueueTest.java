package aufgabe4;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;

public class QueueTest extends ConcurrentTest {
	private static final int TIMEOUT = 5000;
	
	private static class Item {
		public int threadId;
		public int number;
	}
	
	@Test(timeout = TIMEOUT)
	public void testSingleProducerConsumer() throws InterruptedException {
		multiProduceConsume(1, 1, 1000_000);
	}
	
	@Test(timeout = TIMEOUT)
	public void testMultiProducerSingleConsumer() throws InterruptedException {
		multiProduceConsume(2, 1, 1000_000);
	}
	
	@Test(timeout = TIMEOUT)
	public void testSingleProducerMultiConsumer() throws InterruptedException {
		multiProduceConsume(1, 2, 1000_000);
	}
	
	@Test(timeout = TIMEOUT)
	public void testMultiProducersConsumers() throws InterruptedException {
		multiProduceConsume(10, 10, 10_000);
		multiProduceConsume(15, 7, 10_000);
	}
	
	@Test(timeout = TIMEOUT)
	public void testMixedUsage() throws InterruptedException {
		mixedUse(1, 1000_000);
		mixedUse(100, 10_000);
	}

	private void mixedUse(int nofThreads, int nofSteps) throws InterruptedException {
		LockFreeQueue<Integer> queue = new LockFreeQueue<>();
		List<Thread> threads = new ArrayList<>();
		for (int id = 0; id < nofThreads; id++) {
			threads.add(new Thread(() -> {
				for (int number = 0; number < nofSteps; number++) {
					queue.enqueue(number);
					assertNotNull(queue.dequeue());
				}
			}));
		}
		forkJoin(threads);
		assertNull(queue.dequeue());
	}

	private void multiProduceConsume(int nofProducers, int nofConsumers, int elementFraction)
			throws InterruptedException {
		LockFreeQueue<Item> queue = new LockFreeQueue<>();
		List<Thread> threads = new ArrayList<>();
		int totalElements = nofProducers * nofConsumers * elementFraction;
		for (int id = 0; id < nofProducers; id++) {
			int threadId = id;
			threads.add(new Thread(() -> {
				produceItems(totalElements / nofProducers, queue, threadId);
			}));
		}
		for (int id = 0; id < nofConsumers; id++) {
			threads.add(new Thread(() -> {
				consumeItems(totalElements / nofConsumers, queue);
			}));
		}
		forkJoin(threads);
		assertNull(queue.dequeue());
	}

	private void forkJoin(List<Thread> threads) throws InterruptedException {
		for (Thread thread : threads) {
			thread.start();
		}
		for (Thread thread : threads) {
			thread.join();
		}
	}

	private void consumeItems(int amount, LockFreeQueue<Item> queue) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int number = 0; number < amount; number++) {
			Item item = queue.dequeue();
			while (item == null) {
				Thread.yield();
				item = queue.dequeue();
			}
			if (map.containsKey(item.threadId)) {
				assertTrue(map.get(item.threadId) < item.number);
			}
			map.put(item.threadId, item.number);
		}
	}

	private void produceItems(int amount, LockFreeQueue<Item> queue, int threadId) {
		for (int number = 0; number < amount; number++) {
			Item item = new Item();
			item.threadId = threadId;
			item.number = number;
			queue.enqueue(item);
		}
	}
}
