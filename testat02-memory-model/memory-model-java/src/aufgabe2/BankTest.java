package aufgabe2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class BankCustomer extends Thread {
	private static final int NOF_TRANSACTIONS = 1000000;
	private BankAccount account;
	private int maxCredit;

	public BankCustomer(BankAccount account, int maxCredit) {
		this.account = account;
		this.maxCredit = maxCredit;
	}

	public void run() {
		Random random = new Random();
		for (int i = 0; i < NOF_TRANSACTIONS; i++) {
			int amount = random.nextInt(maxCredit);
			account.deposit(amount);
			boolean success = account.withdraw(amount);
			if (!success) {
				throw new AssertionError("Race condition");
			}
		}
	}
}

public class BankTest {
	private final static int NOF_CUSTOMERS = 10;
	private final static int START_BUDGET = 1000;

	public static void main(String[] args) throws InterruptedException {
		BankAccount account = new BankAccount();
		List<BankCustomer> customers = new ArrayList<BankCustomer>();
		Random random = new Random(4711);
		for (int i = 0; i < NOF_CUSTOMERS; i++) {
			customers.add(new BankCustomer(account, random.nextInt(START_BUDGET)));
		}
		account.deposit(START_BUDGET);
		long startTime = System.currentTimeMillis();
		for (BankCustomer c : customers) {
			c.start();
		}
		for (BankCustomer c : customers) {
			c.join();
		}
		long stopTime = System.currentTimeMillis();
		System.out.println("Total time " + (stopTime - startTime) + " ms");
		if (account.getBalance() != START_BUDGET) {
			throw new AssertionError("Incorrect final balance: " + account.getBalance());
		}
	}
}
