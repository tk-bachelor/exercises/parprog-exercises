package theory;

import java.util.concurrent.atomic.AtomicInteger;

class LockFreeSemaphore {
	private AtomicInteger counter;

	public LockFreeSemaphore(int counter) {
		this.counter = new AtomicInteger(counter);
	}

	public void acquire() {
		while(counter.getAndUpdate(i -> i > 0 ? i - 1 : i) <= 0){
			Thread.yield();
		}
	}

	public void release() {
		counter.incrementAndGet();		
	}
}

public class LockFreeSemaphoreTest{
	public static void main(String[] args) {
		
		System.out.println("Let's get started");
		LockFreeSemaphore s = new LockFreeSemaphore(2);
		s.acquire();
		System.out.println(1);
		s.acquire();
		System.out.println(0);
		s.release();
		s.acquire();
		System.out.println("The End");
	}
}
