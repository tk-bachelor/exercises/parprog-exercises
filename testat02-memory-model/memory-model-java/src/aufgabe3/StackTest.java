package aufgabe3;

import java.util.ArrayList;
import java.util.List;

public class StackTest {
	private static final int NOF_THREADS = 100;
	private static final int NOF_STEPS = 100000;
	
	public static void main(String[] args) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		LockFreeStack<Integer> stack = new LockFreeStack<>();
		List<Thread> allThreads = new ArrayList<>();
		for (int i = 0; i < NOF_THREADS; i++) {
			Thread t = new Thread(() -> {
					for (int k = 0; k < NOF_STEPS; k++) {
						stack.push(k);
						if (stack.pop() == null) {
							throw new AssertionError("Race condition");
						}
					}
			});
			allThreads.add(t);
			t.start();
		}
		for (Thread t : allThreads) {
			t.join();
		}
		if (stack.pop() != null) {
			throw new AssertionError("Race condition");
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Duration: "  + (endTime - startTime) + "ms");
	}
}
