package aufgabe3;

import java.util.concurrent.atomic.AtomicReference;

public class LockFreeStack<T> {
	private AtomicReference<Node<T>> top = new AtomicReference<>();

	public void push(T value) {
		top.updateAndGet(current -> new Node<>(value, current));
	}

	public T pop() {
		Node<T> result = top.getAndUpdate(current -> {
			if(current == null){
				return null;
			}
			return current.getNext();			
		});		
		return result == null ? null : result.getValue();
	}
}
