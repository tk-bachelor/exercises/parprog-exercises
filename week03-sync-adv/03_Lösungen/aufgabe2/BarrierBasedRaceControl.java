package aufgabe2;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class BarrierBasedRaceControl extends AbstractRaceControl {
	private CyclicBarrier barrier = new CyclicBarrier(NOF_RACE_CARS + 1);
	private CountDownLatch finished = new CountDownLatch(1);
	private CountDownLatch started = new CountDownLatch(1);
	// other solutions with different combinations of latches/barriers are possible
	
	protected void waitForAllToBeReady() throws InterruptedException {
		try {
			barrier.await();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

	public void readyToStart() {
		try {
			barrier.await();
		} catch (InterruptedException  | BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

	public void waitForStartSignal() throws InterruptedException {
		started.await();
	}

	protected void giveStartSignal() {
		started.countDown();
	}

	protected void waitForFinishing() throws InterruptedException {
		finished.await();
	}

	public boolean isOver() {
		return finished.getCount() == 0;
	}

	public void passFinishLine() {
		finished.countDown();
	}

	public void waitForLapOfHonor() throws InterruptedException {
		try {
			barrier.await();
		} catch (BrokenBarrierException e) {
			throw new AssertionError();
		}
	}
}
