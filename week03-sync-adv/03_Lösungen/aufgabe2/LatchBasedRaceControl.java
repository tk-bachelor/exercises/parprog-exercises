package aufgabe2;

import java.util.concurrent.CountDownLatch;

public class LatchBasedRaceControl extends AbstractRaceControl {
	private CountDownLatch ready = new CountDownLatch(NOF_RACE_CARS);
	private CountDownLatch start = new CountDownLatch(1);
	private CountDownLatch finished = new CountDownLatch(1);
	private CountDownLatch honors = new CountDownLatch(NOF_RACE_CARS);

	protected void waitForAllToBeReady() throws InterruptedException {
		ready.await();
	}

	public void readyToStart() {
		ready.countDown();
	}

	public void waitForStartSignal() throws InterruptedException {
		start.await();
	}

	protected void giveStartSignal() {
		start.countDown();
	}

	protected void waitForFinishing() throws InterruptedException {
		finished.await();
	}

	public boolean isOver() {
		return finished.getCount() == 0;
	}

	public void passFinishLine() {
		finished.countDown();
		honors.countDown();
	}

	public void waitForLapOfHonor() throws InterruptedException {
		honors.await();
	}
}
