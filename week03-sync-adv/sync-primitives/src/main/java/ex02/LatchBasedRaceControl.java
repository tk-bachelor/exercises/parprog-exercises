package ex02;

import java.util.concurrent.CountDownLatch;

public class LatchBasedRaceControl extends AbstractRaceControl {
	private final CountDownLatch carsReady = new CountDownLatch(NOF_RACE_CARS);
	private final CountDownLatch startSignal = new CountDownLatch(1);
	private final CountDownLatch carFinished = new CountDownLatch(1);
	private final CountDownLatch lapOfHonorReady = new CountDownLatch(NOF_RACE_CARS);
	

	@Override
	protected void waitForAllToBeReady() throws InterruptedException {
		carsReady.await();
	}

	@Override
	public void readyToStart() {
		carsReady.countDown();
	}
	
	@Override
	public void waitForStartSignal() throws InterruptedException {
		startSignal.await();
	}

	@Override
	protected void giveStartSignal() {
		startSignal.countDown();
	}	

	@Override
	protected void waitForFinishing() throws InterruptedException {
		carFinished.await();
	}

	@Override
	public boolean isOver() {
		return carFinished.getCount() == 0;
	}

	@Override
	public void passFinishLine() {
		carFinished.countDown();
		lapOfHonorReady.countDown();
	}

	@Override
	public void waitForLapOfHonor() throws InterruptedException {
		lapOfHonorReady.await();
	}

}
