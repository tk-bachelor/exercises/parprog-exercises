package ex02;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class BarrierBasedRaceControl extends AbstractRaceControl {
	private final CyclicBarrier barrier = new CyclicBarrier(NOF_RACE_CARS + 1);
	private final CountDownLatch startSignal = new CountDownLatch(1);
	private final CountDownLatch finished = new CountDownLatch(1);
	
	@Override
	protected void waitForAllToBeReady() throws InterruptedException {
		try {
			barrier.await();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void readyToStart() {
		try {
			barrier.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void waitForStartSignal() throws InterruptedException {
		startSignal.await();
	}

	@Override
	protected void giveStartSignal() {
		startSignal.countDown();
	}

	@Override
	protected void waitForFinishing() throws InterruptedException {
		finished.await();
	}

	@Override
	public boolean isOver() {
		return finished.getCount() == 0;
	}

	@Override
	public void passFinishLine() {
		finished.countDown();		
	}

	@Override
	public void waitForLapOfHonor() throws InterruptedException {
		try {
			barrier.await();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

}
