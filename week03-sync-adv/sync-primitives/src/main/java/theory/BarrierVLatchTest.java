package theory;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class BarrierVLatchTest {
	private CyclicBarrier barrier = new CyclicBarrier(3);
	private CountDownLatch latch = new CountDownLatch(3);
	private CountDownLatch startSignal = new CountDownLatch(1);
	
	public void prepare() throws InterruptedException, BrokenBarrierException{
		barrier.await();
	}
	
	public void prepareLatch() throws InterruptedException{		
		latch.countDown();
		startSignal.await();
	}
	
	public static void main(String[] args) throws InterruptedException {
		BarrierVLatchTest t = new BarrierVLatchTest();
		t.prepareLatch();
	}
	
	
}
