package ex01;

public class WarehouseWithMonitor implements Warehouse {
	private int stock = 0;
	private final int capacity;

	public WarehouseWithMonitor(int capacity) {
		this.capacity = capacity;
	}

	@Override
	public synchronized void put(int amount) throws InterruptedException {
		while(stock + amount > capacity){
			wait();
		}
		stock += amount;
		notifyAll();
	}

	@Override
	public synchronized void get(int amount) throws InterruptedException {
		while(stock < amount){
			wait();
		}
		stock -= amount;
		notifyAll();
	}
}
