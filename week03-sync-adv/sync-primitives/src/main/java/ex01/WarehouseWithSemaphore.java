package ex01;

import java.util.concurrent.Semaphore;

public class WarehouseWithSemaphore implements Warehouse {
	private int stock = 0;
	private final Semaphore remainingCapacity;
	private final Semaphore occupiedCapacity;
	private final Semaphore mutex;

	public WarehouseWithSemaphore(int capacity, boolean fair) {
		remainingCapacity = new Semaphore(capacity, fair);
		occupiedCapacity = new Semaphore(0, fair);
		mutex = new Semaphore(1, fair);
	}

	@Override
	public void put(int amount) throws InterruptedException {
		remainingCapacity.acquire(amount);
		mutex.acquire();
		stock += amount;
		mutex.release();
		occupiedCapacity.release(amount);
	}

	@Override
	public void get(int amount) throws InterruptedException {
		occupiedCapacity.acquire(amount);
		mutex.acquire();
		stock -= amount;
		mutex.release();
		remainingCapacity.release(amount);
	}
}
