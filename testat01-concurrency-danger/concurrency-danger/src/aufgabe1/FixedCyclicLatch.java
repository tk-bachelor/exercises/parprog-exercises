package aufgabe1;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class FixedCyclicLatch {
	private static final int NOF_ROUNDS = 10;
	private static final int NOF_THREADS = 10;

	private static CyclicBarrier barrier = new CyclicBarrier(NOF_THREADS);

	private static void multiRounds(int threadId) 
			throws InterruptedException, BrokenBarrierException {
		for (int round = 0; round < NOF_ROUNDS; round++) {
			barrier.await();
			System.out.println("Round " + round + " thread " + threadId);
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i < NOF_THREADS; i++) {
			final int threadId = i;
			new Thread(() -> {
				try {
					multiRounds(threadId);
				} catch (InterruptedException | BrokenBarrierException e) {
					throw new AssertionError();
				}
			}).start();
		}
	}
}
