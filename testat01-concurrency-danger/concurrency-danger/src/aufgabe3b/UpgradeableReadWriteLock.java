package aufgabe3b;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class UpgradeableReadWriteLock {
	private ReadWriteLock rwLock = new ReentrantReadWriteLock();
	private Lock reentrantMutex = new ReentrantLock();

	public void readLock() throws InterruptedException {		
		rwLock.readLock().lock();
	}

	public void readUnlock() throws InterruptedException {
		rwLock.readLock().unlock();
	}

	public void upgradeableReadLock() throws InterruptedException {
		reentrantMutex.lock();
	}

	public void upgradeableReadUnlock() {
		reentrantMutex.unlock();
	}

	public void writeLock() throws InterruptedException {
		reentrantMutex.lock();
		rwLock.writeLock().lock();
	}

	public void writeUnlock() {		
		rwLock.writeLock().unlock();
		reentrantMutex.unlock();
	}
}
