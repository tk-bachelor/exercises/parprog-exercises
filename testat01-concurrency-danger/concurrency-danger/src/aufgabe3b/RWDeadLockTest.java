package aufgabe3b;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RWDeadLockTest {

	public static void main(String[] args) throws InterruptedException {
		rwDeadlock();
	}

	private static void urwDeadlock() throws InterruptedException {
		UpgradeableReadWriteLock urwLock = new UpgradeableReadWriteLock();

		System.out.println("Enter Readlock");
		urwLock.upgradeableReadLock();
		System.out.println("inside readlock");

		// upgrade to writelock
		urwLock.writeLock();
		System.out.println("inside writelock");
		urwLock.writeUnlock();
		urwLock.upgradeableReadUnlock();
		System.out.println("exit Readlock");
	}

	private static void rwDeadlock() {
		ReadWriteLock rwLock = new ReentrantReadWriteLock(true);

		System.out.println("ready to enter readlock");
		rwLock.readLock().lock();
		System.out.println("inside readlock");

		// deadlock
		System.out.println("ready to enter writelock");
		rwLock.writeLock().lock();
		rwLock.writeLock().unlock();
		System.out.println("exited writelock");
		
		rwLock.readLock().unlock();
		System.out.println("exited readlock");
	}
}
