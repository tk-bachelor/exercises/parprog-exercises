package se;


public class TimeFormatter {
	
	public static void main(String[] args) {
		System.out.println(hoursInFloatToString(3.20f));
		System.out.println(hoursInFloatToString(0.20f));
		System.out.println(hoursInFloatToString(-2.7f));
	}
	
	public static String hoursInFloatToString( float f ) {
		int hours = (int)f;
		int minutes = Math.round((f-hours) * 60);
		StringBuilder sb = new StringBuilder();
		if (hours > 0) {
		sb.append( hours );
		sb.append( "h" );
		if (minutes < 10) {
		sb.append( "0" );
		}
		sb.append( minutes );
		}
		else {
		sb.append( minutes );
		sb.append( "min." );
		}
		return sb.toString();
		}
}
