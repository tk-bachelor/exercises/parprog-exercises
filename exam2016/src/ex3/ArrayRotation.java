package ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

public class ArrayRotation {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ArrayRotation rotation = new ArrayRotation();
		rotation.rotate();
		
		rotation.rotateThreadPool();
	}

	public void rotate() {
		int[] array = { 1, 2, 3, 4, 5 };
		int last = array[array.length - 1];

		for (int i = 0; i < array.length; i++) {
			// swap
			int temp = last;
			last = array[i];
			array[i] = temp;
		}

		this.print(array);
	}

	public void rotateThreadPool() throws InterruptedException, ExecutionException {
		int[] array = { 1, 2, 3, 4, 5 };
		int[] output = new int[array.length];

		ForkJoinPool threadPool = ForkJoinPool.commonPool();

		List<Future<?>> futures = new ArrayList<>();

		for (int i = 0; i < array.length; i++) {
			int index = i;
			futures.add(threadPool.submit(() -> {
				output[index] = array[(index + array.length - 1) % array.length];
			}));
		}

		for(Future<?> f : futures) {
			f.get();
		}
		
		this.print(output);
	}

	private void print(int[] array) {
		for (int i : array) {
			System.out.println(i);
		}
	}

}
