package ex2;

import java.util.concurrent.atomic.AtomicReference;

public class LockFreeStack<T> {
	private class Node {
		T value;
		Node next;
	}

	private final AtomicReference<Node> top = new AtomicReference<>();

	public void push(T value) {
		Node node = new Node();
		node.value = value;
		do {
			node.next = top.get();
		} while (!top.compareAndSet(node.next, node));
	}

	public T pop() {
		Node node;
		do {
			node = top.get();
			if (node == null) {
				return null;
			}
		} while (!top.compareAndSet(node, node.next));
		return node.value;
	}
}
