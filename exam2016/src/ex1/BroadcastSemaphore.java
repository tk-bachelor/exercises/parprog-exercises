package ex1;

import java.util.concurrent.Semaphore;

public class BroadcastSemaphore<T> {
	private final int amount;
	private T exchange;
	private Semaphore receivers = new Semaphore(0);
	private Semaphore receivingItem = new Semaphore(0);

	public BroadcastSemaphore(int amount) {
		this.amount = amount;
	}

	public void send(T item) throws InterruptedException {
		if (item == null) {
			throw new IllegalArgumentException();
		}
		exchange = item;
		receivingItem.release(amount);
		receivers.acquire(amount);
	}

	public synchronized T receive() throws InterruptedException {
		receivingItem.acquire();
		receivers.release();
		return exchange;
	}
}
