package ex1;

public class Broadcast<T> {
	private final int amount;
	private T exchange;
	private int receivers = 0;

	public Broadcast(int amount) {
		this.amount = amount;
	}

	public synchronized void send(T item) throws InterruptedException {
		if (item == null) {
			throw new IllegalArgumentException();
		}
		exchange = item;
		notifyAll();

		while (receivers < amount) {
			wait();
		}
	}

	public synchronized T receive() throws InterruptedException {
		if (exchange == null) {
			wait();
		}
		receivers++;
		notifyAll();
		return exchange;
	}
}
