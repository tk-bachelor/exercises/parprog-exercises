package aufgabe2b;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class BankAccount {
	private int balance = 0;

	public synchronized void deposit(int amount) {
		balance += amount;
		notifyAll();
	}

	// returns true if successful, false if failed after timeout.
	public synchronized boolean withdraw(int amount, long timeOutMillis) throws InterruptedException {
		if (timeOutMillis < 0) {
			throw new IllegalArgumentException("timeOutMillis is negative");
		}
		long startTime = System.currentTimeMillis();
		long currentTime = startTime;
		while (amount > balance && currentTime - startTime < timeOutMillis) {
			// wait timeout period must be positive: therefore fix currentTime in temporary variable
			wait(timeOutMillis - currentTime + startTime);
			currentTime = System.currentTimeMillis();
		}
		if (amount <= balance) {
			balance -= amount;
			return true;
		} else {
			return false;
		}
	}

	public synchronized int getBalance() {
		return balance;
	}
}

class BankCreditCustomer extends Thread {
	private static final int NOF_TRANSACTIONS = 100;
	private final BankAccount account;
	private int amount;

	public BankCreditCustomer(BankAccount account, int amount) {
		this.account = account;
		this.amount = amount;
	}

	@Override
	public void run() {
		for (int i = 0; i < NOF_TRANSACTIONS; i++) {
			boolean success;
			try {
				success = account.withdraw(amount, 1);
				if (success) {
					System.out.println("Use credit " + amount + " by " + Thread.currentThread().getName());
					account.deposit(amount);
				} else {
					System.out.println("Credit time out " + amount + " by " + Thread.currentThread().getName());
				}
			} catch (InterruptedException e) {
				throw new AssertionError();
			}
		}
	}
}

public class BankTest2 {
	private static final int NOF_CUSTOMERS = 10;
	private static final int START_BUDGET = 1000;

	public static void main(String[] args) throws InterruptedException {
		BankAccount account = new BankAccount();
		List<BankCreditCustomer> customers = new ArrayList<>();
		Random random = new Random(4711);
		for (int i = 0; i < NOF_CUSTOMERS; i++) {
			customers.add(new BankCreditCustomer(account, random.nextInt(1000)));
		}
		for (BankCreditCustomer customer : customers) {
			customer.start();
		}
		account.deposit(START_BUDGET);
		for (BankCreditCustomer customer : customers) {
			customer.join();
		}
		if (account.getBalance() != START_BUDGET) {
			throw new AssertionError("Incorrect final balance: " + account.getBalance());
		}
	}
}
