package parprog.week3.ex4;

class Room {
	private final int LIMIT = 5;
	private int currentOccupation = 0;

	public synchronized void enter() {
		while (currentOccupation == LIMIT) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		currentOccupation++;
		System.out.println("+++++++++++++ current occupation: "+currentOccupation);
	}

	public synchronized void exit() {
		currentOccupation--;
		System.out.println("------------- current occupation: "+currentOccupation);
		
		// notify is enough, because the condition is same for all
		// but it is not fair, because one arbitrary waiting thread may continue
		notify();
	}
}

public class RoomOccupation {
	public static void main(String[] args) {
		
		// shared resource
		Room r = new Room();
		
		// create threads for 10 people
		for(int i = 0; i < 10; i++){
			
				// create and start thread
				new Thread(() -> {			
				
						r.enter();
						System.out.println("Room entered. " + Thread.currentThread().getName());
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						r.exit();
						System.out.println("Room exited" + Thread.currentThread().getName());
				}, "Person "+i).start();			
		}
	}
}
