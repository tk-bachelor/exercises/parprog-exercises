package parprog.week2.ex2b;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class BankAccount {
	private int balance = 0;

	public synchronized void deposit(int amount) {
		balance += amount;
		notifyAll();
	}

	public synchronized boolean withdraw(int amount, long timeout) throws InterruptedException {
		if (timeout < 0) {
			throw new IllegalArgumentException("timeout should be positive");
		}
		long startTime = System.currentTimeMillis();
		long currentTime = startTime;
		long elapsedTime = timeout;

		// timeout should be checked in loop condition every time		
		while (balance < amount && (elapsedTime = currentTime - startTime) < timeout) {
            // recalculate remaining time
            // otherwise thread goes to wait 100ms every time it will be awaken again and again.
			wait(timeout - elapsedTime);

            // the currentTime must be reset
			currentTime = System.currentTimeMillis();
		}

        // after timeout the following code will be executed
		// balance should be checked again because of limited waiting time
		if (balance >= amount) {
			balance -= amount;
			return true;
		} else {
			return false;
		}

	}

	public synchronized int getBalance() {
		return balance;
	}
}

class BankCreditCustomer extends Thread {
	private static final int NOF_TRANSACTIONS = 100;
	private final BankAccount account;
	private final int amount;

	public BankCreditCustomer(BankAccount account, int amount) {
		this.account = account;
		this.amount = amount;
	}

	@Override
	public void run() {
		for (int i = 0; i < NOF_TRANSACTIONS; i++) {
			try {
				boolean success = account.withdraw(amount, 1);
				if (success) {
					System.out.println("Use credit " + amount + " by " + Thread.currentThread().getName());
					account.deposit(amount);
				} else {
					System.out.println("timeout exceeded");
				}
			} catch (InterruptedException es) {
				throw new AssertionError();
			}
		}
	}
}

public class BankTest2 {
	private static final int NOF_CUSTOMERS = 10;
	private static final int START_BUDGET = 1000;

	public static void main(String[] args) throws InterruptedException {
		BankAccount account = new BankAccount();
		List<BankCreditCustomer> customers = new ArrayList<>();
		Random random = new Random(4711);
		for (int i = 0; i < NOF_CUSTOMERS; i++) {
			customers.add(new BankCreditCustomer(account, random.nextInt(1000)));
		}
		for (BankCreditCustomer customer : customers) {
			customer.start();
		}
		account.deposit(START_BUDGET);
		for (BankCreditCustomer customer : customers) {
			customer.join();
		}
		if (account.getBalance() != START_BUDGET) {
			throw new AssertionError("Incorrect final balance: " + account.getBalance());
		}
	}
}
