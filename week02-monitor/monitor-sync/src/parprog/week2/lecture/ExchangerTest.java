package parprog.week2.lecture;

import java.util.concurrent.Exchanger;

public class ExchangerTest {

	public static void main(String[] args) {
		Exchanger<Integer> exchanger = new Exchanger<>();
		for (int k = 0; k < 2; k++) {
			new Thread(() -> {
				for (int in = 0; in < 5; in++) {
					int out;
					try {
						out = exchanger.exchange(in);
						System.out.println(Thread.currentThread().getName() + " got " + out);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}).start();
		}
	}

}
