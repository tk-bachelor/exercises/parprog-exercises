package parprog.week2.ex2a;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class BankAccount {
	private int balance = 0;

	public synchronized void deposit(int amount) {
		balance += amount;
		notifyAll();
	}

	public synchronized void withdraw(int amount) throws InterruptedException {
		while (balance < amount) {
			wait();
		}
		amount -= balance;
	}

	public synchronized int getBalance() {
		return balance;
	}
}

class BankCreditCustomer extends Thread {
	private static final int NOF_TRANSACTIONS = 100;
	private final BankAccount account;
	private final int amount;

	public BankCreditCustomer(BankAccount account, int amount) {
		this.account = account;
		this.amount = amount;
	}

	@Override
	public void run() {
		for (int i = 0; i < NOF_TRANSACTIONS; i++) {
			System.out.println(
					"Balance: " + account.getBalance() + " - " + amount + " by " + Thread.currentThread().getName());

			// >>> Critical Section
			// while (account.getBalance() < amount) {
			// Thread.yield();
			// }
			// account.withdraw(amount);
			// System.out.println("Use credit " + amount + " by " +
			// Thread.currentThread().getName());
			// account.deposit(amount);

			// >>> Critical Section
			// TODO ask professor
			try {
				synchronized (account) {
					account.withdraw(amount);
					System.out.println("Use credit " + amount + " by " + Thread.currentThread().getName());
					account.deposit(amount);
				}				
			} catch (InterruptedException e) {
				throw new AssertionError(Thread.currentThread().getName() + " was interrupted");
			}
		}
	}
}

public class BankTest2 {
	private static final int NOF_CUSTOMERS = 10;
	private static final int START_BUDGET = 1000;

	public static void main(String[] args) throws InterruptedException {
		BankAccount account = new BankAccount();
		List<BankCreditCustomer> customers = new ArrayList<>();
		Random random = new Random(4711);
		for (int i = 0; i < NOF_CUSTOMERS; i++) {
			customers.add(new BankCreditCustomer(account, random.nextInt(1000)));
		}
		for (BankCreditCustomer customer : customers) {
			customer.start();
		}
		account.deposit(START_BUDGET);
		for (BankCreditCustomer customer : customers) {
			customer.join();
		}

		if (account.getBalance() != START_BUDGET) {
			throw new AssertionError("Incorrect final balance: " + account.getBalance());
		}
	}
}
