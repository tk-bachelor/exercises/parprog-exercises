package parprog.week2.ex1;

import java.util.ArrayList;
import java.util.List;

class BankAccount2 {
	private int balance = 0;

	public synchronized void deposit(int amount) {
		balance += amount;
	}

	public synchronized boolean withdraw(int amount) {
		if (amount <= balance) {
			balance -= amount;
			return true;
		} else {
			return false;
		}
	}

	public synchronized int getBalance() {
		return balance;
	}
}

class BankCustomer2 extends Thread {
	private static final int NOF_TRANSACTIONS = 10000000;
	private final BankAccount2 account;

	public BankCustomer2(BankAccount2 account) {
		this.account = account;
	}

	@Override
	public void run() {
		for (int k = 0; k < NOF_TRANSACTIONS; k++) {
			account.deposit(100);
			boolean success = account.withdraw(100);
			if (!success) {
				throw new AssertionError(
						"deposited amount couldn't be withdrawn --> race condition should have been occurred");
			}
		}
	}
}

public class BankTest2 {
	private static final int NOF_CUSTOMERS = 10;

	public static void main(String[] args) {
		BankAccount2 account = new BankAccount2();

		List<BankCustomer2> customers = new ArrayList<>();
		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < NOF_CUSTOMERS; i++) {
			BankCustomer2 c = new BankCustomer2(account);
			customers.add(c);
			c.start();
		}

		customers.forEach(c -> {
			try {
				c.join();
			} catch (InterruptedException e) {
				System.err.println("Thread interrupted");
			}
		});
		
		long endTime = System.currentTimeMillis();
		System.out.println(" Time: " + (endTime - startTime) + " ms");
		
		System.out.println(account.getBalance());
		if (account.getBalance() != 0) {		
			throw new AssertionError("balance is not 0 --> race condition should have been occurred");
		}
	}
}
