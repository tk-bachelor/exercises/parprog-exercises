package task1;

import java.time.LocalDate;
import scala.concurrent.stm.Ref;
import scala.concurrent.stm.japi.STM;

public class Account {
	private final Ref.View<Integer> balance = STM.newRef(0);
	private final Ref.View<LocalDate> lastUpdate = STM.newRef(LocalDate.now());
	private final Ref.View<Boolean> isClosed = STM.newRef(false);
	
	public void withdraw(int amount) {
		STM.atomic(() -> {
			if (isClosed.get()) {
				throw new RuntimeException("Closed account");
			}
			if (balance.get() < amount) {
				STM.retry();
			}
			STM.increment(balance,  -amount);
			lastUpdate.set(LocalDate.now());
		});
	}

	public void deposit(int amount) {
		STM.atomic(() -> {
			if (isClosed.get()) {
				throw new RuntimeException("Closed account");
			}
			STM.increment(balance, amount);
			lastUpdate.set(LocalDate.now());
		});
	}

	public void setClosed(boolean isClosed) {
		this.isClosed.set(isClosed);
	}

	public int getBalance() {
		return balance.get();
	}

	public LocalDate getLastUpdate() {
		return lastUpdate.get();
	}

	public static void transfer(Account from, Account to, int amount) {
		STM.atomic(() -> {
			from.withdraw(amount);
			to.deposit(amount);			
		});
	}
}
