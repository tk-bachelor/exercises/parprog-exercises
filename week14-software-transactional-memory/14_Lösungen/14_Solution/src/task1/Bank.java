package task1;

import java.util.Map;

import scala.concurrent.stm.japi.STM;

public class Bank {
	private final Map<String, Account> accounts = STM.newMap();

	public Account openAccount(String name) {
		return STM.atomic(() -> {
			if (getAccount(name) != null) {
				throw new RuntimeException("Account already exists");
			}
			Account account = new Account();
			accounts.put(name, account);
			return account;
		});
	}

	public int nofAccounts() {
		return accounts.size();
	}
	
	public Account getAccount(String name) {
		return accounts.get(name);
	}
}
