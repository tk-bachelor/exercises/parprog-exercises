package aufgabe1;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

class QuickSortTask extends RecursiveAction {
	private static final long serialVersionUID = 1L;
	private static final int THRESHOLD = 100_000;

	private int[] array;
	private int left, right;

	public QuickSortTask(int[] array, int left, int right) {
		this.array = array;
		this.left = left;
		this.right = right;
	}

	// sorts the partition between array[l] and array[r]
	private void quickSort(int l, int r) {
		// split into two partitions
		int i = l, j = r;
		long m = array[(l + r) / 2];
		do {
			while (array[i] < m) {
				i++;
			}
			while (array[j] > m) {
				j--;
			}
			if (i <= j) {
				int t = array[i];
				array[i] = array[j];
				array[j] = t;
				i++;
				j--;
			}
		} while (i <= j);
		// recursively sort the two partitions
		if (j - l > THRESHOLD && r - i > THRESHOLD) {
			invokeAll(new QuickSortTask(array, l, j), new QuickSortTask(array, i, r));
		} else {
			if (j > l) {
				quickSort(l, j);
			}
			if (i < r) {
				quickSort(i, r);
			}
		}
	}

	protected void compute() {
		quickSort(left, right);
	}
}

public class QuickSortSample {
	private static final int NOF_ELEMENTS = 50_000_000;

	private static int[] createRandomArray(int length) {
		Random random = new Random(4711);
		int[] numberArray = new int[length];
		for (int i = 0; i < length; i++) {
			numberArray[i] = random.nextInt();
		}
		return numberArray;
	}

	private static void checkSorted(int[] numberArray) {
		for (int i = 0; i < numberArray.length - 1; i++) {
			if (numberArray[i] > numberArray[i + 1]) {
				throw new RuntimeException("Not sorted");
			}
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			int[] numberArray = createRandomArray(NOF_ELEMENTS);
			long startTime = System.currentTimeMillis();
			ForkJoinPool pool = new ForkJoinPool();
			pool.invoke(new QuickSortTask(numberArray, 0, NOF_ELEMENTS - 1));
			long stopTime = System.currentTimeMillis();
			System.out.println("Total time: " + (stopTime - startTime) + " ms");
			checkSorted(numberArray);
		}
	}
}
