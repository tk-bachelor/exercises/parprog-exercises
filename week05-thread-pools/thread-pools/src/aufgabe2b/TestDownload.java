package aufgabe2b;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class TestDownload {
	private static final String[] links = new String[] { "https://www.google.com", "https://www.bing.com",
			"https://www.yahoo.com", "https://www.microsoft.com", "https://www.oracle.com" };

	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		long startTime = System.currentTimeMillis();
		WebDownload downloader = new WebDownload();

		CompletableFuture<Void> all = CompletableFuture.runAsync(() -> {
			System.out.println("All CompletableFuture started");
		});

		for (int i = 0; i < links.length; i++) {
			final String link = links[i];
			CompletableFuture<Void> future = downloader.asyncDownloadUrl(links[i]).thenAccept(result -> {
				// callee-centric approach (push)
				long endTime = System.currentTimeMillis();
				System.out.println(String.format("%s downloaded (%d characters) after %d ms", link, result.length(),
						endTime - startTime));
			});
			all = CompletableFuture.allOf(all, future);
		}
		all.thenAccept(voids -> {
			long endTime = System.currentTimeMillis();
			System.out.println(String.format("total time: %d ms", endTime - startTime));
		}).join();

	}
}
