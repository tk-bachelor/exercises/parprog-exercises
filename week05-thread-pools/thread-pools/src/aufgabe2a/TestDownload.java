package aufgabe2a;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class TestDownload {
	private static final String[] links = new String[] { "https://www.google.com", "https://www.bing.com",
			"https://www.yahoo.com", "https://www.microsoft.com", "https://www.oracle.com" };

	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		long startTime = System.currentTimeMillis();
		WebDownload downloader = new WebDownload();
		List<Future<String>> futures = new ArrayList<>();
		for (int i = 0; i < links.length; i++) {
			futures.add(downloader.asyncDownloadUrl(links[i]));
		}
		for (int i = 0; i < links.length; i++) {
			
			// caller centric approach (pull)
			String result = futures.get(i).get();
			System.out.println(String.format("%s downloaded (%d characters)", links[i], result.length()));
		}
		long endTime = System.currentTimeMillis();
		System.out.println(String.format("total time: %d ms", endTime - startTime));
	}
}
