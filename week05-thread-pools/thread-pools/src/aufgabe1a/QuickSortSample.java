package aufgabe1a;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

class QuickSortTask extends RecursiveAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final int THRESH_HOLD = 100_000;

	private int[] array;
	private int left, right;

	public QuickSortTask(int[] array, int left, int right) {
		this.array = array;
		this.left = left;
		this.right = right;
	}

	// sorts the partition between array[left] and array[right]
	private void quickSort(int partitionLeftLimit, int partitionRightLimit) {
		// split into two partitions
		int leftPointer = partitionLeftLimit, rightPointer = partitionRightLimit;
		long middlePartionPos = array[(partitionLeftLimit + partitionRightLimit) / 2];
		do {
			while (array[leftPointer] < middlePartionPos) {
				leftPointer++;
			}
			while (array[rightPointer] > middlePartionPos) {
				rightPointer--;
			}
			if (leftPointer <= rightPointer) {
				int t = array[leftPointer];
				array[leftPointer] = array[rightPointer];
				array[rightPointer] = t;
				leftPointer++;
				rightPointer--;
			}
		} while (leftPointer <= rightPointer);
		// recursively sort the two partitions

		if (rightPointer - partitionLeftLimit > THRESH_HOLD 
				&& partitionRightLimit - leftPointer > THRESH_HOLD) {
			invokeAll(new QuickSortTask(array, partitionLeftLimit, rightPointer),
					new QuickSortTask(array, leftPointer, partitionRightLimit));
		} else {
			if (rightPointer > partitionLeftLimit) {
				quickSort(partitionLeftLimit, rightPointer);
			}
			if (leftPointer < partitionRightLimit) {
				quickSort(leftPointer, partitionRightLimit);
			}
		}
	}

	@Override
	protected void compute() {
		quickSort(left, right);
	}

}

public class QuickSortSample {
	private static final int NOF_ELEMENTS = 50_000_000;

	private static int[] createRandomArray(int length) {
		Random random = new Random(4711);
		int[] numberArray = new int[length];
		for (int i = 0; i < length; i++) {
			numberArray[i] = random.nextInt();
		}
		return numberArray;
	}

	private static void checkSorted(int[] numberArray) {
		for (int i = 0; i < numberArray.length - 1; i++) {
			if (numberArray[i] > numberArray[i + 1]) {
				throw new RuntimeException("Not sorted");
			}
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			int[] numberArray = createRandomArray(NOF_ELEMENTS);
			long startTime = System.currentTimeMillis();

			ForkJoinPool pool = new ForkJoinPool();
			pool.invoke(new QuickSortTask(numberArray, 0, NOF_ELEMENTS - 1));

			long stopTime = System.currentTimeMillis();
			System.out.println("Total time: " + (stopTime - startTime) + " ms");
			checkSorted(numberArray);
		}
	}
}
