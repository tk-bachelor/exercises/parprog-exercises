package theory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class MaxSearchTaskTest {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		int[] array = { 3, 6, 1, 25, 2, 245, 234, 1, 23, 13, 45, 34, 25, 2, 52, 23, 41, 62, 5, 2, 12, 2, 5, 5, 24, 23 };

		ForkJoinPool pool = new ForkJoinPool();
		// Future<Integer> future = pool.submit(new MaxSearchTask(array, 0,
		// array.length));^
		int max = pool.invoke(new MaxSearchTask(array, 0, array.length));

		System.out.println(max);
	}

	public static int getMaximum(int[] array) {
		int max = Integer.MIN_VALUE;
		for (int value : array) {
			max = Math.max(value, max);
		}
		return max;
	}

	public static int getMaximum(int[] array, int left, int right) {
		int max = Integer.MIN_VALUE;
		for (int i = left; i < right; i++) {
			max = Math.max(array[i], max);
		}
		return max;
	}
}

class MaxSearchTask extends RecursiveTask<Integer> {
	public static final int THRESHHOLD = 2;

	private int[] array;
	private int left, right;

	public MaxSearchTask(int[] array, int left, int right) {
		this.array = array;
		this.left = left;
		this.right = right;
	}

	@Override
	protected Integer compute() {
		if (right - left >= THRESHHOLD) {
			int middle = (right + left) / 2;
			ForkJoinTask<Integer> leftTask = new MaxSearchTask(array, left, middle).fork();
			MaxSearchTask rightTask = new MaxSearchTask(array, middle, right);
			rightTask.fork();
			return Math.max(leftTask.join(), rightTask.join());
		} else {
			return MaxSearchTaskTest.getMaximum(array, left, right);
		}
	}
}
