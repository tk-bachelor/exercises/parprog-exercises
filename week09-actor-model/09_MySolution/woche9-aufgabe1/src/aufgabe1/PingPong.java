package aufgabe1;

import java.util.concurrent.TimeUnit;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

public class PingPong {

	static final FiniteDuration FIVE_SECONDS = Duration.create(5, TimeUnit.SECONDS);

	public static void main(String[] args) {
		ActorSystem system = ActorSystem.create("PingPong");

		ActorRef p1 = system.actorOf(Props.create(PingPongActor.class), "P1");
		ActorRef p2 = system.actorOf(Props.create(PingPongActor.class), "P2");

		/*
		 * indem wir p2 als Sender angeben kann p1 mit getSender darauf
		 * zugreifen
		 */
		p1.tell(new Start(), p2);

		// send a reset message
		system.scheduler().schedule(FIVE_SECONDS, FIVE_SECONDS, p1, new Reset(), system.dispatcher(),
				ActorRef.noSender());
	}

	static class Start {
	}

	static class Reset {
	}

	static class Ping {
		final int count;

		public Ping(int count) {
			this.count = count;
		}
	}

	static class PingPongActor extends UntypedActor {

		private boolean reset = false;

		public void onReceive(Object message) {
			if (message instanceof Start) {
				handleStart((Start) message);
			} else if (message instanceof Reset) {
				handleReset((Reset) message);
			} else if (message instanceof Ping) {
				handlePing((Ping) message);
			}
		}

		private void handleReset(Reset message) {
			reset = true;
		}

		private void handlePing(Ping msg) {
			System.out.println(getSelf().path().name() + ": count " + msg.count);
			try {
				Thread.sleep((long) (Math.random() * 1000) + 300);
			} catch (InterruptedException e) {
			}

			if (reset) {
				reset = false;
				getSender().tell(new Ping(0), getSelf());
			} else if (msg.count == 10) {
				getContext().system().terminate().value();
			} else {
				// TODO increment the counter and send it back to the sender
				getSender().tell(new Ping(msg.count + 1), getSelf());
			}
		}

		private void handleStart(Start message) {
			System.out.println("Starting...");

			// TODO Set counter on 0 and tell the sender
			getSender().tell(new Ping(0), getSelf());
		}
	}
}