﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace DownloadPerformance
{
    class Program
    {
        public static void Main(string[] args)
        {
            new Program()._MeasurePerformance();
        }

        private void _MeasurePerformance()
        {
            var watch = Stopwatch.StartNew();
            var downloads = new Task[] {
                _DownloadWebsiteAsync("http://www.google.com"),
                _DownloadWebsiteAsync("http://www.bing.com"),
                _DownloadWebsiteAsync("http://www.yahoo.com"),
                _DownloadWebsiteAsync("http://msdn.microsoft.com"),
                _DownloadWebsiteAsync("http://www.facebook.com"),
                _DownloadWebsiteAsync("http://www.xing.com")
            };
            Task.WhenAll(downloads).ContinueWith(predecessors =>
               Console.WriteLine($"Elapsed {watch.ElapsedMilliseconds} ms")).Wait();
        }

        private static Task _DownloadWebsiteAsync(string url)
        {
            return Task.Run(() =>
            {
                var watch = Stopwatch.StartNew();
                var client = new HttpClient();
                var html = client.GetStringAsync(url).Result;
                Console.WriteLine($"{url} downloaded (length {html.Length}): {watch.ElapsedMilliseconds} ms");
            });
        }
    }
}
