﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace DownloadPerformance
{
    class Program
    {
        public static void Main()
        {
            new Program().MeasurePerformance();
        }

        private void MeasurePerformance()
        {
            var watch = Stopwatch.StartNew();
            var downloads = new Task[]
            {
                DownloadWebsite("http://www.google.com"),
                DownloadWebsite("http://www.bing.com"),
                DownloadWebsite("http://www.yahoo.com"),
                DownloadWebsite("http://msdn.microsoft.com"),
                DownloadWebsite("http://www.facebook.com"),
                DownloadWebsite("http://www.xing.com")
            };
            Task.WhenAll(downloads).ContinueWith(predecessors =>
            {
                Console.WriteLine($"Elapsed {watch.ElapsedMilliseconds} ms");
            }).Wait();
            Console.ReadLine();
        }

        private static Task DownloadWebsite(string url)
        {
            return Task.Run(() =>
            {
                var watch = Stopwatch.StartNew();
                var client = new HttpClient();
                var html = client.GetStringAsync(url).Result;
                Console.WriteLine($"{url} downloaded (length {html.Length}): {watch.ElapsedMilliseconds} ms");
            });
        }
    }
}
